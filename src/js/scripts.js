//
'use strict';
  // Open tabs
  function openTab(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = jQuery('.tabcontent');
    //
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = 'none';
    }
    tablinks = jQuery('.tablinks');
    //
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(' active', '');
    }
    //
    jQuery('#'+tabName).css('display', 'block');
    jQuery(evt.currentTarget).addClass('active');
  }
//
(function ($, window, document, Mustache, undefined) {

  // Load hotels data
  function loadHotels() {
    var deferred = new $.Deferred();
    $.get('data/hotels.json', function(hotelsData) {
      var template = $('#hotels-tpl').html();
      Mustache.parse(template);   // optional, speeds up future uses
      var rendered = Mustache.render(template, hotelsData);
      $('#hotels-target').html(rendered);
      //
      deferred.resolve();
    });
    //
    return deferred.promise();
  }
  // Load cars data
  function loadCars() {
    var deferred = new $.Deferred();    
    $.get('data/cars.json', function(carsData) {
      var template = $('#cars-tpl').html();
      Mustache.parse(template);   // optional, speeds up future uses
      var rendered = Mustache.render(template, carsData);
      $('#cars-target').html(rendered);
      //
      deferred.resolve();
    });
    //
    return deferred.promise();
  }
  // This function is used to collapse and uncollapse expander
  function initExpander(){
    //
    $('.expander').click(function(){
      var contentOne = $(this).parent().find('.card-content');
      $(this).find('img').toggleClass('closed');
      $(contentOne).slideToggle('slow');
    });
  }
  $(function () {
    // Load Data and render templates
    loadHotels().then(loadCars).then(initExpander);
    // Default tab is Hotel
    openTab(event, 'Hotels');
    $('#btn-hotel').addClass('active');
  });

})(jQuery, window, document, Mustache);
